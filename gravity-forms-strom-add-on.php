<?php
/*
Plugin Name: Gravity Forms Strom Add-On
Plugin URI: http://www.gravityforms.com
Description: A simple add-on to demonstrate how to use the Add-On Framework to include a new field type.
Version: 1.2.1
Author: Bradford Knowlton
Author URI: http://www.bradknowlton.com
Text Domain: simplefieldaddon
Domain Path: /languages

------------------------------------------------------------------------
Copyright 2012-2017 Bradford Knowlton

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
*
* Changelog
* 
* 1.1.5 Initial Version
* 1.1.6 Added Editer permissions for Gravity Forms
* 1.1.7 Imported WooCommerce User Meta Boxes
* 1.2.1 Added B&W PDF functionality
*
**/

define( 'GF_SIMPLE_FIELD_ADDON_VERSION', '1.1.7' );

define( 'GF_SIMPLE_FIELD_ADDON_PATH', plugin_dir_path( __FILE__ ) );

add_action( 'gform_loaded', array( 'GF_Simple_Field_AddOn_Bootstrap', 'load' ), 5 );

class GF_Simple_Field_AddOn_Bootstrap {

    public static function load() {

        if ( ! method_exists( 'GFForms', 'include_addon_framework' ) ) {
            return;
        }

        require_once( 'class-gfsimplefieldaddon.php' );

        GFAddOn::register( 'GFSimpleFieldAddOn' );
    }

}

if( !function_exists('create_document_type_taxonomies') ){
		
	// create two taxonomies, genres and writers for the post type "book"
	function create_document_type_taxonomies() {
		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => _x( 'Document Type', 'taxonomy general name', 'textdomain' ),
			'singular_name'     => _x( 'Document Type', 'taxonomy singular name', 'textdomain' ),
			'search_items'      => __( 'Search Document Types', 'textdomain' ),
			'all_items'         => __( 'All Document Types', 'textdomain' ),
			'parent_item'       => __( 'Parent Document Type', 'textdomain' ),
			'parent_item_colon' => __( 'Parent Document Type:', 'textdomain' ),
			'edit_item'         => __( 'Edit Document Type', 'textdomain' ),
			'update_item'       => __( 'Update Document Type', 'textdomain' ),
			'add_new_item'      => __( 'Add New Document Type', 'textdomain' ),
			'new_item_name'     => __( 'New Document Type', 'textdomain' ),
			'menu_name'         => __( 'Document Types', 'textdomain' ),
		);
	
		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => true,
		);
	
		register_taxonomy( 'document-type', null, $args );
		
		
		$labels = array(
			'name'              => _x( 'Document Status', 'taxonomy general name', 'textdomain' ),
			'singular_name'     => _x( 'Document Status', 'taxonomy singular name', 'textdomain' ),
			'search_items'      => __( 'Search Document Status', 'textdomain' ),
			'all_items'         => __( 'All Document Status', 'textdomain' ),
			'parent_item'       => __( 'Parent Document Status', 'textdomain' ),
			'parent_item_colon' => __( 'Parent Document Status:', 'textdomain' ),
			'edit_item'         => __( 'Edit Document Status', 'textdomain' ),
			'update_item'       => __( 'Update Document Status', 'textdomain' ),
			'add_new_item'      => __( 'Add New Document Status', 'textdomain' ),
			'new_item_name'     => __( 'New Document Status', 'textdomain' ),
			'menu_name'         => __( 'Document Status', 'textdomain' ),
		);
	
		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => true,
		);
	
		register_taxonomy( 'document-status', null, $args );
		
		
	} // end function create_document_type_taxonomies
	
	// hook into the init action and call create_book_taxonomies when it fires
	add_action( 'init', 'create_document_type_taxonomies', 0 );
	
} // end function_exists create_document_type_taxonomies

if( !function_exists( 'custom_menu_items' )){

	function custom_menu_items() {
		
		add_submenu_page('gf_edit_forms', 'Document Types', 'Document Types', 'manage_options', 'edit-tags.php?taxonomy=document-type',false );
		
		add_submenu_page('gf_edit_forms', 'Document Status', 'Document Status', 'manage_options', 'edit-tags.php?taxonomy=document-status',false );
		
		add_submenu_page('gf_entries', 'Document Types', 'Document Types', 'editor', 'edit-tags.php?taxonomy=document-type',false );
		
		add_submenu_page('gf_entries', 'Document Status', 'Document Status', 'editor', 'edit-tags.php?taxonomy=document-status',false );
	
	}
	
	/**  */
	add_action( 'admin_menu', 'custom_menu_items', 11 );

} // end function_exists( 'custom_menu_items' )

if ( ! function_exists( 'mbe_set_current_menu' ) ) {

    function mbe_set_current_menu( $parent_file ) {
        global $submenu_file, $current_screen, $pagenow;

        # Set the submenu as active/current while anywhere in your Custom Post Type (nwcm_news)
        if ( $current_screen->taxonomy == 'document-type' ) {
            $parent_file = 'gf_edit_forms';
        }else if ( $current_screen->taxonomy == 'document-status' ) {
            $parent_file = 'gf_edit_forms';
        }
        
         # Set the submenu as active/current while anywhere in your Custom Post Type (nwcm_news)
        if ( $current_screen->taxonomy == 'document-type' && current_user_can( 'editor' ) ) {
            $parent_file = 'gf_entries';
        }else if ( $current_screen->taxonomy == 'document-status' && current_user_can( 'editor' ) ) {
            $parent_file = 'gf_entries';
        }

        return $parent_file;
    }

    add_filter( 'parent_file', 'mbe_set_current_menu' );

} // end function_exists( 'mbe_set_current_menu' )



if( ! function_exists( 'us_states' ) ){

	function us_states( $states ) {
	    $new_states = array();
	    foreach ( $states as $state ) {
	       $new_states[ GF_Fields::get( 'address' )->get_us_state_code( $state ) ] = $state;
	       // $new_states[ $state ] = $state;
	    }

	    return $new_states;
	}

	add_filter( 'gform_us_states', 'us_states' );

}


add_filter( 'gform_is_value_match', function ( $is_match, $field_value, $target_value, $operation, $source_field, $rule ) {
	
    if ( $source_field->type == 'docuploader' ) {
    
    	if( '' == $field_value && ( 'approved' == $target_value || 'pending' == $target_value ) && 'is' == $operation ){		
			  return true;
		}
		
		if( ! is_array( $field_value ) ){
		
			$ary = explode( '|:|', $field_value );
			
			// pending, declined, accepted etc
			$status = count( $ary ) > 4 ? $ary[5] : '';
						
			return RGFormsModel::matches_operation( $status, $target_value, $operation );			
		}
		
    }
 
    return $is_match;
}, 10, 6 );



if( ! function_exists( 'gv_enable_gf_notifications_after_update' )){

	/**
	 * Triggers Gravity Forms notifications engine when entry is updated (admin or frontend)
	 * @param  array $form    GF form
	 * @param  int $entry_id  Lead/entry id
	 * @return void
	 */
	function gv_enable_gf_notifications_after_update( $form, $entry_id ) {

		if( !is_admin() || !class_exists('GFCommon') || !class_exists( 'GFAPI' ) ) {
			return;
		}

		$entry = GFAPI::get_entry( $entry_id );

		GFCommon::send_form_submission_notifications( $form, $entry );

	}

	add_action( 'gform_after_update_entry', 'gv_enable_gf_notifications_after_update', 10, 2 );

}


add_filter('gettext', 'woo_translate_reply');
add_filter('ngettext', 'woo_translate_reply');
function woo_translate_reply($translated) {
	$translated = str_ireplace('Company', 'Move Identification', $translated);
	$translated = str_ireplace('Customer billing address', 'Customer home address', $translated);
	$translated = str_ireplace('Customer shipping address', 'Customer move address', $translated);
	$translated = str_ireplace('Copy from billing address', 'Copy from home address', $translated);
	
	//  Phrase used in multiple places
	//	$translated = str_ireplace('First name', 'First name', $translated);
	//	$translated = str_ireplace('Last name', 'Last name', $translated);

	return $translated;
}


/**
 * Add all Gravity Forms capabilities to Editor role.
 * Runs during plugin activation.
 * 
 * @access public
 * @return void
 */
function activate_pluginname() {
  
  
  // https://isabelcastillo.com/editor-role-manage-users-wordpress
    $edit_editor = get_role('editor'); // Get the user role
    $edit_editor->add_cap('edit_users');
    $edit_editor->add_cap('list_users');
    $edit_editor->add_cap('promote_users');
    $edit_editor->add_cap('create_users');
    $edit_editor->add_cap('add_users');
    // $edit_editor->add_cap('delete_users');

	// $edit_editor->add_cap( 'gravityforms_edit_forms' );
	$edit_editor->add_cap( 'gravityforms_view_entries' );
	$edit_editor->add_cap( 'gf_entries' );
	
	$edit_editor->add_cap( 'manage_woocommerce' );
	
}
// Register our activation hook
register_activation_hook( __FILE__, 'activate_pluginname' );

/**
 * Remove Gravity Forms capabilities from Editor role.
 * Runs during plugin deactivation.
 * 
 * @access public
 * @return void
 */
function deactivate_pluginname() {
 
  // https://isabelcastillo.com/editor-role-manage-users-wordpress
    $edit_editor = get_role('editor'); // Get the user role
    $edit_editor->remove_cap('edit_users');
    $edit_editor->remove_cap('list_users');
    $edit_editor->remove_cap('promote_users');
    $edit_editor->remove_cap('create_users');
    $edit_editor->remove_cap('add_users');
    // $edit_editor->add_cap('delete_users');

	$edit_editor->remove_cap( 'gform_full_access' );
	$edit_editor->remove_cap( 'gravityforms_edit_forms' );
	
	$edit_editor->remove_cap( 'gravityforms_view_entries' );
	$edit_editor->remove_cap( 'gf_entries' );
	
	$edit_editor->remove_cap( 'manage_woocommerce' ); 

}
// Register our de-activation hook
register_deactivation_hook( __FILE__, 'deactivate_pluginname' );

// https://isabelcastillo.com/editor-role-manage-users-wordpress
add_action( 'admin_init', 'my_remove_menu_pages', 9999 );
function my_remove_menu_pages() {

    global $user_ID;

    if ( current_user_can( 'editor' ) ) {
		remove_menu_page('edit.php?post_type=thirstylink');
		remove_menu_page('edit.php?post_type=wprss_feed');
        remove_menu_page('authorhreview' );
		remove_menu_page('edit.php'); // Posts
		// remove_menu_page('upload.php'); // Media
		remove_menu_page('link-manager.php'); // Links
		remove_menu_page('edit-comments.php'); // Comments
		remove_menu_page('edit.php?post_type=page'); // Pages
		remove_menu_page('plugins.php'); // Plugins
		remove_menu_page('themes.php'); // Appearance
		// remove_menu_page('users.php'); // Users
		remove_menu_page('tools.php'); // Tools
		remove_menu_page('options-general.php'); // Settings
		
		remove_menu_page('edit.php?post_type=shop_order'); // WooCommerce
		remove_menu_page('edit.php?post_type=product'); // Products
		remove_menu_page('admin.php?page=wc-settings'); // WooCommerce
		remove_menu_page('admin.php?page=wc-status'); // WooCommerce
		remove_menu_page('admin.php?page=wc-addons'); // WooCommerce
		remove_menu_page('wc-reports'); // WooCommerce
		remove_menu_page('WooCommerce'); // WooCommerce
		remove_menu_page('woocommerce'); // WooCommerce
		remove_menu_page('woocommerce'); // WooCommerce
		remove_menu_page('separator-woocommerce'); // WooCommerce
		
		
    }
}

/*
	Disable Default Dashboard Widgets
	@ https://digwp.com/2014/02/disable-default-dashboard-widgets/
*/
function disable_default_dashboard_widgets() {
	global $wp_meta_boxes;
	// wp..
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	// bbpress
	unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);
	// yoast seo
	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);
	// gravity forms
	// unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);
}
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets', 999);

// https://isabelcastillo.com/editor-role-manage-users-wordpress

/*
 * Let Editors manage users, and run this only once.
 */
function isa_editor_manage_users() {
 
    if ( get_option( 'isa_add_cap_editor_once' ) != 'done' ) {
     
        // let editor manage users
 
        $edit_editor = get_role('editor'); // Get the user role
        $edit_editor->add_cap('edit_users');
        $edit_editor->add_cap('list_users');
        $edit_editor->add_cap('promote_users');
        $edit_editor->add_cap('create_users');
        $edit_editor->add_cap('add_users');
        // $edit_editor->add_cap('delete_users');
 
        update_option( 'isa_add_cap_editor_once', 'done' );
    }
 
}
add_action( 'init', 'isa_editor_manage_users' );


// https://codex.wordpress.org/Dashboard_Widgets_API#Advanced:_Removing_Dashboard_Widgets

// Create the function to use in the action hook
function example_remove_dashboard_widget() {
 	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
} 
 
// Hook into the 'wp_dashboard_setup' action to register our function
// add_action('wp_dashboard_setup', 'example_remove_dashboard_widget' );

function remove_dashboard_meta() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
		// wpe_dify_news_feed
		remove_meta_box( 'wpe_dify_news_feed', 'dashboard', 'normal');//since 3.8
		remove_meta_box( 'wpe_dify_news_feed', 'dashboard', 'side');//since 3.8
		
		// https://gist.github.com/RiaanKnoetze/1532254adbef7a08e0453cf65810dfa2
		
		// remove WooCommerce Dashboard Status
		remove_meta_box( 'woocommerce_dashboard_status', 'dashboard', 'normal'); 
}
add_action( 'admin_init', 'remove_dashboard_meta' );


// https://wordpress.stackexchange.com/questions/126922/remove-post-from-new-menu-in-top-bar
// 
function hide_add_new() {
global $submenu;
    // For Removing New Posts from Admin Menu
    unset($submenu['post-new.php?post_type=post'][10]);
    // For Removing New Pages
    unset($submenu['post-new.php?post_type=page'][10]);
   // For Removing CPTs
    unset($submenu['post-new.php?post_type=custom_post_type'][10]);
}
add_action('admin_menu', 'hide_add_new');

//Thanks to Howdy_McGee
function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('new-post');
    $wp_admin_bar->remove_menu('new-page');
    $wp_admin_bar->remove_menu('new-media');
	// gravityforms-new-form
	$wp_admin_bar->remove_menu('new-form');
	$wp_admin_bar->remove_menu('gf-new-form');
	$wp_admin_bar->remove_menu('gravityforms-new-form');
	// WooCommerce
	$wp_admin_bar->remove_menu('new-product');
	$wp_admin_bar->remove_menu('new-order');
	$wp_admin_bar->remove_menu('new-coupon');
	$wp_admin_bar->remove_menu('view-store');
	
	$wp_admin_bar->remove_menu('shop_coupon');
	$wp_admin_bar->remove_menu('shop_product');
	

}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links', 9999 );


function disable_new_post() {
if ( get_current_screen()->post_type == 'my_post_type' )
    wp_die( "You ain't allowed to do that!" );
}
add_action( 'load-post-new.php', 'disable_new_post' );

// 
// https://wordpress.stackexchange.com/questions/178678/how-to-remove-comments-option-from-wp-admin-bar-and-modify-profile-icon

function my_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'my_admin_bar_render' );

function modify_settings_icon() {
    global $menu;
    $menu[80][6] = 'dashicons-admin-site';
}
add_action( 'admin_menu', 'modify_settings_icon' );

// https://codex.wordpress.org/Function_Reference/remove_node
// 
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}



// https://www.gravityhelp.com/forums/topic/hide-the-help-section


if( ! function_exists( 'gf_editor_remove_help' ) ){

	//set the priority so this is called after the menu had been built
	add_action('admin_menu',  'gf_editor_remove_help', 9999);
	function gf_editor_remove_help()
	{
		//the first parameter is the parent name of the first item in the Forms menu
		remove_submenu_page("gf_entries","gf_help");
	}
	
}	