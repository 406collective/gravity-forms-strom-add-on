<?php

if ( ! class_exists( 'GFForms' ) ) {
	die();
}

class Doc_Uploader_GF_Field extends GF_Field_FileUpload {

	/**
	 * @var string $type The field type.
	 */
	public $type = 'docuploader';
	
	public $displayUrl = false;
	
	public $displayType = true;
	
	public $displayStatus = false;

	/**
	 * Return the field title, for use in the form editor.
	 *
	 * @return string
	 */
	public function get_form_editor_field_title() {
		return esc_attr__( 'Doc Uploader', 'simplefieldaddon' );
	}
	
	/**
	 * Defines if conditional logic is supported by the Name field.
	 *
	 * @since Unknown
	 * @access public
	 *
	 * @used-by GFFormDetail::inline_scripts()
	 * @used-by GFFormSettings::output_field_scripts()
	 *
	 * @return bool true
	 */
	public function is_conditional_logic_supported() {
		return true;
	}

	/**
	 * Assign the field button to the Advanced Fields group.
	 *
	 * @return array
	 */
	public function get_form_editor_button() {
		return array(
			'group' => 'advanced_fields',
			'text'  => $this->get_form_editor_field_title(),
		);
	}

		function get_form_editor_field_settings() {
		return array(
			'conditional_logic_field_setting',
			'error_message_setting',
			'label_setting',
			// 'encrypt_setting',
			'label_placement_setting',
			'admin_label_setting',
			'file_extensions_setting',
			'file_size_setting',
			// 'post_image_setting',
			'rules_setting',
			'description_setting',
			'css_class_setting',
			'size_setting',
			// 'post_image_featured_image',
		);
	}

	public function get_field_input( $form, $value = '', $entry = null ) {

		$form_id         = $form['id'];
		$is_entry_detail = $this->is_entry_detail();
		$is_form_editor  = $this->is_form_editor();
		$is_admin = $is_entry_detail || $is_form_editor;

		$id       = (int) $this->id;
		$field_id = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";

		$size         = $this->size;
		$class_suffix = $is_entry_detail ? '_admin' : '';
		$class        = $size . $class_suffix;

		$disabled_text = $is_form_editor ? 'disabled="disabled"' : '';
		
		if( ! is_array( $value ) ){
		
			$ary = explode( '|:|', $value );
			
			$value = array();
			$value[$this->id . '.0'] = count( $ary ) > 0 ? $ary[0] : '';
			$value[$this->id . '.1'] = count( $ary ) > 1 ? $ary[1] : '';
			$value[$this->id . '.4'] = count( $ary ) > 2 ? $ary[2] : '';
			$value[$this->id . '.7'] = count( $ary ) > 3 ? $ary[3] : '';
			
			// type
			$value[$this->id . '.10'] = count( $ary ) > 4 ? $ary[4] : '';
			// status
			$value[$this->id . '.11'] = count( $ary ) > 5 ? $ary[5] : '';
			
			$value[$this->id . '.12'] = count( $ary ) > 6 ? $ary[6] : '';
			
			$value[$this->id . '.13'] = count( $ary ) > 7 ? $ary[7] : ''; // secondary id
			
		}
		
		$url       = esc_attr( rgget( $this->id . '.0', $value ) );
		
		if( $is_entry_detail && ! $url ){
			return '';
		}
		
		$title       = esc_attr( rgget( $this->id . '.1', $value ) );
		$caption     = esc_attr( rgget( $this->id . '.4', $value ) );
		$description = esc_attr( rgget( $this->id . '.7', $value ) );
		
		$type = esc_attr( rgget( $this->id . '.10', $value ) );
		$status = esc_attr( rgget( $this->id . '.11', $value ) );
		$post_id = esc_attr( rgget( $this->id . '.12', $value ) );
		
		$secondary_id = esc_attr( rgget( $this->id . '.13', $value ) ); // secondary

		//hiding meta fields for admin
		$hidden_style      	= "style='display:none;'";
		$url_style 			= ! $this->displayUrl && $is_admin ? $hidden_style : '';
		$title_style       	= ! $this->displayTitle && $is_admin ? $hidden_style : '';
		$caption_style     	= ! $this->displayCaption && $is_admin ? $hidden_style : '';
		$description_style 	= ! $this->displayDescription && $is_admin ? $hidden_style : '';
		$type_style   	  	= ! $this->displayType && $is_admin ? $hidden_style : '';
		$status_style 		= ! $is_admin ? $hidden_style : '';
		
		$file_label_style  = $is_admin && ! ( $this->displayTitle || $this->displayCaption || $this->displayDescription ) ? $hidden_style : '';

		$hidden_class = $preview = '';
		$file_info    = RGFormsModel::get_temp_filename( $form_id, "input_{$id}" );
		if ( $file_info ) {
			$hidden_class     = ' gform_hidden';
			$file_label_style = $hidden_style;
			$preview          = "<span class='ginput_preview'><strong>" . esc_html( $file_info['uploaded_filename'] ) . "</strong> | <a href='javascript:;' onclick='gformDeleteUploadedFile({$form_id}, {$id});' onkeypress='gformDeleteUploadedFile({$form_id}, {$id});'>" . __( 'delete', 'gravityforms' ) . '</a></span>';
		}

		//in admin, render all meta fields to allow for immediate feedback, but hide the ones not selected
		$file_label = ( $is_admin || $this->displayTitle || $this->displayCaption || $this->displayDescription ) ? "<label for='$field_id' class='ginput_post_image_file' $file_label_style>" . gf_apply_filters( array( 'gform_postimage_file', $form_id ), __( 'File', 'gravityforms' ), $form_id ) . '</label>' : '';

		$tabindex = $this->get_tabindex();

		$upload = ! $this->is_entry_detail() ? sprintf( "<span class='ginput_left$class_suffix'>{$preview}<input name='input_%d' id='%s' type='file' class='%s' $tabindex %s/>$file_label</span>", $id, $field_id, esc_attr( $class . $hidden_class ), $disabled_text ): '';

		$url_field = $this->displayUrl || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_url' $url_style><input type='text' name='input_%d.0' id='%s_0' value='%s' $tabindex %s/><label for='%s_0'>" . gf_apply_filters( array( 'gform_postimage_title', $form_id ), __( 'Url', 'gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $url, $disabled_text, $field_id ) : '';
		
		$img_tag = $this->displayUrl || $is_admin ? "<span class='ginput_left$class_suffix ginput_post_image_title'><a href='$url' target='_blank' title='" . __( 'Click to view', 'gravityforms' ) . "'><img src='".wp_get_attachment_image_src($post_id, 'medium')[0]."' width='".wp_get_attachment_image_src($post_id, 'medium')[1]."' /></a></span>" : '';

		$img_field = $this->displayUrl || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_id' $url_style><input type='text' name='input_%d.12' id='%s_12' value='%s' $tabindex %s/><label for='%s_12'>" . gf_apply_filters( array( 'gform_postimage_title', $form_id ), __( 'ID', 'gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $post_id, $disabled_text, $field_id ) : '';

		$tabindex = $this->get_tabindex();

		$title_field = $this->displayTitle || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_title' $title_style><input type='text' name='input_%d.1' id='%s_1' value='%s' $tabindex %s/><label for='%s_1'>" . gf_apply_filters( array( 'gform_postimage_title', $form_id ), __( 'Title', 'gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $title, $disabled_text, $field_id ) : '';

		$tabindex = $this->get_tabindex();

		$caption_field = $this->displayCaption || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_caption' $caption_style><input type='text' name='input_%d.4' id='%s_4' value='%s' $tabindex %s/><label for='%s_4'>" . gf_apply_filters( array( 'gform_postimage_caption', $form_id ), __( 'Caption', 'gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $caption, $disabled_text, $field_id ) : '';

		$tabindex = $this->get_tabindex();

		$description_field = $this->displayDescription || $is_admin ? sprintf( "<span class='ginput_full$class_suffix ginput_post_image_description' $description_style><input type='text' name='input_%d.7' id='%s_7' value='%s' $tabindex %s/><label for='%s_7'>" . gf_apply_filters( array( 'gform_postimage_description', $form_id ), __( 'Description', 'gravityforms' ), $form_id ) . '</label></span>', $id, $field_id, $description, $disabled_text, $field_id ) : '';

		$tabindex = $this->get_tabindex();
		
		// <input type='text' name='input_%d.7' id='%s_7' value='%s' $tabindex %s/><label for='%s_7'>
		
		$args = array(
				'show_option_all'    => '',
				'show_option_none'   => '',
				'option_none_value'  => '-1',
				'orderby'            => 'ID',
				'order'              => 'ASC',
				'show_count'         => 0,
				'hide_empty'         => 0,
				'child_of'           => 0,
				'exclude'            => '',
				'include'            => '',
				'echo'               => 0,
				'selected'           => $type,
				'hierarchical'       => 0,
				'name'               => 'input_'.$id.'.10',
				'id'                 => $field_id.'_10',
				'class'              => esc_attr( $class ).' medium',
				'depth'              => 0,
				'tab_index'          => $tabindex,
				'taxonomy'           => 'document-type',
				'hide_if_empty'      => false,
				'value_field'	     => 'slug',
			);

		$type_field = $this->displayType || $is_admin ? sprintf( "<span class='ginput_left$class_suffix ginput_post_image_description' $type_style>". wp_dropdown_categories( $args ) . "<label for='%s_10'>" . gf_apply_filters( array( 'gform_postimage_type', $form_id ), __( 'Type', 'gravityforms' ), $form_id ) . '</label></span>', $field_id ) : '';

		$tabindex = $this->get_tabindex();

		$args = array(
				'show_option_all'    => '',
				'show_option_none'   => '',
				'option_none_value'  => '-1',
				'orderby'            => 'ID',
				'order'              => 'ASC',
				'show_count'         => 0,
				'hide_empty'         => 0,
				'child_of'           => 0,
				'exclude'            => '',
				'include'            => '',
				'echo'               => 0,
				'selected'           => $status,
				'hierarchical'       => 0,
				'name'               => 'input_'.$id.'.11',
				'id'                 => $field_id.'_11',
				'class'              => esc_attr( $class ).' medium',
				'depth'              => 0,
				'tab_index'          => $tabindex,
				'taxonomy'           => 'document-status',
				'hide_if_empty'      => false,
				'value_field'	     => 'slug',
			);

		$status_field = $this->displayStatus || $is_admin ? sprintf( "<span class='ginput_right$class_suffix ginput_post_image_description' $status_style>".wp_dropdown_categories( $args )."<label for='%s_11'>" . gf_apply_filters( array( 'gform_postimage_status', $form_id ), __( 'Status', 'gravityforms' ), $form_id ) . '</label></span>', $field_id ) : '';


		return "<div class='ginput_complex$class_suffix ginput_container ginput_container_post_image'>" . $upload . $img_tag . $img_field . $url_field .  $title_field . $caption_field . $description_field . $type_field . $status_field . '</div>';
	}
	
	
	
	
	
	/**
	*
	*
	*
	*
	*/
	public function get_value_save_entry( $value, $form, $input_name, $lead_id, $lead ) {
		$form_id = $form['id'];
		$is_entry_detail = $this->is_entry_detail();
		$is_form_editor  = $this->is_form_editor();
		
		$is_admin = $is_entry_detail || $is_form_editor;
		
		if( $is_admin ){
			// file upload not set
			
			$url = isset( $_POST["{$input_name}_0"] ) ? wp_strip_all_tags( $_POST["{$input_name}_0"] ) : '';
			
			if ( ! GFCommon::is_valid_url( $url ) ) {
				GFCommon::log_debug( __METHOD__ . '(): aborting; File URL invalid.' );
	
				return '';
			}
			
			$attach_id = isset( $_POST["{$input_name}_12"] ) ? wp_strip_all_tags( $_POST["{$input_name}_12"] ) : ''; 
			
		}else{
		
			// $url     = $this->get_single_file_value( $form_id, $input_name );
			
			global $_gf_uploaded_files;
			GFCommon::log_debug( __METHOD__ . '(): Starting.' );
			if ( empty( $_gf_uploaded_files ) ) {
				$_gf_uploaded_files = array();
			}
			if ( ! isset( $_gf_uploaded_files[ $input_name ] ) ) {
				//check if file has already been uploaded by previous step
				$file_info     = GFFormsModel::get_temp_filename( $form_id, $input_name );
				$temp_filepath = GFFormsModel::get_upload_path( $form_id ) . '/tmp/' . $file_info['temp_filename'];
				if ( $file_info && file_exists( $temp_filepath ) ) {
					GFCommon::log_debug( __METHOD__ . '(): File already uploaded to tmp folder, moving.' );
					$_gf_uploaded_files[ $input_name ] = $this->move_temp_file( $form_id, $file_info );
				} else if ( ! empty( $_FILES[ $input_name ]['name'] ) ) {
					GFCommon::log_debug( __METHOD__ . '(): calling upload_file' );
					$target = $this->upload_file( $form_id, $_FILES[ $input_name ] );
					
					$_gf_uploaded_files[ $input_name ] = $target['url'];
					
				} else {
					GFCommon::log_debug( __METHOD__ . '(): No file uploaded. Exiting.' );
				}
			}
			
			$url = rgget( $input_name, $_gf_uploaded_files );
			
			
	
			if ( empty( $url ) ) {
				return '';
			}
	
			if ( ! GFCommon::is_valid_url( $url ) ) {
				GFCommon::log_debug( __METHOD__ . '(): aborting; File URL invalid.' );
	
				return '';
			}
			
			// var_dump($target);
			
			// $filename should be the path to a file in the upload directory.
			$filename = $target['path'];
			
			// The ID of the post this attachment is for.
			$parent_post_id = 0;
			
			// Check the type of file. We'll use this as the 'post_mime_type'.
			$filetype = wp_check_filetype( basename( $filename ), null );
			
			// Get the path to the upload directory.
			// $wp_upload_dir = wp_upload_dir();
			
			// var_dump($wp_upload_dir);
			
			// Prepare an array of post data for the attachment.
			$attachment = array(
				'guid'           => $target['url'],  //$wp_upload_dir['url'] . '/' . basename( $filename )
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content'   => '',
				'post_status'    => 'draft'
			);
			
			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );
			
			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			
			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			
			// convert images to pdf
			
			
			
			// var_dump($filetype['type']);
			
			if( stristr( $filetype['type'], "image" ) ){
			
				GFCommon::log_debug( __METHOD__ . '(): Starting PDF Conversion.' );
				
				$image = new Imagick($filename);
				
				// If 0 is provided as a width or height parameter,
				// aspect ratio is maintained
				// $image->thumbnailImage(100, 0);
				
				$image->setFormat("pdf");
				
				$image->setImageColorSpace(Imagick::COLORSPACE_GRAY); 
				
				// imagick::RESOLUTION_PIXELSPERINCH
				
				$image->setImageUnits( Imagick::RESOLUTION_PIXELSPERINCH ); 
				
				$image->setImageResolution(300,300);

				
				
				// var_dump($_FILES[ $input_name ]);
				
				$file = $_FILES[ $input_name ]['name'];
				
				$info = pathinfo($file);
				
				$target = GFFormsModel::get_file_upload_path( $form_id, $info['filename'].".pdf" );
				
				// var_dump($target['path']);
				
				$fileHandle = fopen( $target['path'], "w" );
				$image->writeImageFile( $fileHandle );
				fclose($fileHandle);
				// echo $target['url'];
				
				// header('Content-type: application/pdf');
				// echo $image;
				
				$image->clear();
				$image->destroy(); 
				
				// The ID of the post this attachment is for.
				$parent_post_id = 0;
				
				// Check the type of file. We'll use this as the 'post_mime_type'.
				$filetype = wp_check_filetype( basename( $target['path'] ), null );
				
				// Get the path to the upload directory.
				// $wp_upload_dir = wp_upload_dir();
				
				// var_dump($wp_upload_dir);
				
				// Prepare an array of post data for the attachment.
				$attachment = array(
					'guid'           => $target['url'],  //$wp_upload_dir['url'] . '/' . basename( $filename )
					'post_mime_type' => $filetype['type'],
					'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $target['path'] ) ),
					'post_content'   => '',
					'post_status'    => 'draft'
				);
				
				// Insert the attachment.
				$secondary_attach_id = wp_insert_attachment( $attachment, $target['path'], $parent_post_id );
				
				// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				
				// Generate the metadata for the attachment, and update the database record.
				$attach_data = wp_generate_attachment_metadata( $secondary_attach_id, $target['path'] );
				wp_update_attachment_metadata( $secondary_attach_id, $attach_data );
				
			}else{
				$secondary_attach_id = $attach_id;
			}
			
			
			// wp_die();
			// die();			
		}
		
		$image_title       = isset( $_POST["{$input_name}_1"] ) ? wp_strip_all_tags( $_POST["{$input_name}_1"] ) : '';
		$image_caption     = isset( $_POST["{$input_name}_4"] ) ? wp_strip_all_tags( $_POST["{$input_name}_4"] ) : '';
		$image_description = isset( $_POST["{$input_name}_7"] ) ? wp_strip_all_tags( $_POST["{$input_name}_7"] ) : '';
		
		$image_type = isset( $_POST["{$input_name}_10"] ) ? wp_strip_all_tags( $_POST["{$input_name}_10"] ) : '';
		
		$image_status = isset( $_POST["{$input_name}_11"] ) ? wp_strip_all_tags( $_POST["{$input_name}_11"] ) : 'pending';
		
		$image_id = $attach_id;
		
		$image_secondary_id = $secondary_attach_id;

		return $url . '|:|' . $image_title . '|:|' . $image_caption . '|:|' . $image_description . '|:|' . $image_type . '|:|' . $image_status . '|:|' . $image_id . '|:|' . $image_secondary_id;
	}

	public function upload_file( $form_id, $file ) {
		GFCommon::log_debug( __METHOD__ . '(): Uploading file: ' . $file['name'] );
		$target = GFFormsModel::get_file_upload_path( $form_id, $file['name'] );
		if ( ! $target ) {
			GFCommon::log_debug( __METHOD__ . '(): FAILED (Upload folder could not be created.)' );
			return 'FAILED (Upload folder could not be created.)';
		}
		GFCommon::log_debug( __METHOD__ . '(): Upload folder is ' . print_r( $target, true ) );
		if ( move_uploaded_file( $file['tmp_name'], $target['path'] ) ) {
			GFCommon::log_debug( __METHOD__ . '(): File ' . $file['tmp_name'] . ' successfully moved to ' . $target['path'] . '.' );
			$this->set_permissions( $target['path'] );
			return $target; // ['url'] // return complete target variable
		} else {
			GFCommon::log_debug( __METHOD__ . '(): FAILED (Temporary file ' . $file['tmp_name'] . ' could not be copied to ' . $target['path'] . '.)' );
			return 'FAILED (Temporary file could not be copied.)';
		}
	}

	public function move_temp_file( $form_id, $tempfile_info ) {
		$target = GFFormsModel::get_file_upload_path( $form_id, $tempfile_info['uploaded_filename'] );
		$source = GFFormsModel::get_upload_path( $form_id ) . '/tmp/' . $tempfile_info['temp_filename'];
		GFCommon::log_debug( __METHOD__ . '(): Moving temp file from: ' . $source );
		if ( rename( $source, $target['path'] ) ) {
			GFCommon::log_debug( __METHOD__ . '(): File successfully moved.' );
			$this->set_permissions( $target['path'] );
			return $target['url'];
		} else {
			GFCommon::log_debug( __METHOD__ . '(): FAILED (Temporary file could not be moved.)' );
			return 'FAILED (Temporary file could not be moved.)';
		}
	}
	
	public function set_permissions( $path ) {
		GFCommon::log_debug( __METHOD__ . '(): Setting permissions on: ' . $path );
		GFFormsModel::set_permissions( $path );
	}

	
	public function get_value_entry_list( $value, $entry, $field_id, $columns, $form ) {
		list( $url, $title, $caption, $description, $type, $status, $id ) = rgexplode( '|:|', $value, 7 );			
		if ( ! empty( $url ) ) {
			//displaying thumbnail (if file is an image) or an icon based on the extension
			$thumb = GFEntryList::get_icon_url( $url );
			$value = "<a href='" . esc_attr( $url ) . "' target='_blank' title='" . __( 'Click to view', 'gravityforms' ) . "'><img src='$thumb'/></a>";
		}
		return $value;
	}


	/**
	*
	*
	*
	*
	*/
	public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {
		$ary         	= explode( '|:|', $value );
		$url         	= count( $ary ) > 0 ? $ary[0] : '';
		$title       	= count( $ary ) > 1 ? $ary[1] : '';
		$caption     	= count( $ary ) > 2 ? $ary[2] : '';
		$description 	= count( $ary ) > 3 ? $ary[3] : '';
		$type 		 	= count( $ary ) > 4 ? $ary[4] : '';
		$status 	 	= count( $ary ) > 5 ? $ary[5] : '';
		$id 	 	 	= count( $ary ) > 6 ? $ary[6] : '';
		$secondary_id 	= count( $ary ) > 7 ? $ary[7] : '';

		if ( ! empty( $url ) ) {
			$url = str_replace( ' ', '%20', $url );

			switch ( $format ) {
				case 'text' :
					$value = $url;
					$value .= ! empty( $title ) ? "\n\n" . $this->label . ' (' . __( 'Title', 'gravityforms' ) . '): ' . $title : '';
					$value .= ! empty( $caption ) ? "\n\n" . $this->label . ' (' . __( 'Caption', 'gravityforms' ) . '): ' . $caption : '';
					$value .= ! empty( $description ) ? "\n\n" . $this->label . ' (' . __( 'Description', 'gravityforms' ) . '): ' . $description : '';
					
					$type = get_term_by('slug', $type, 'document-type')->name;
					
					$value .= ! empty( $type ) ? "\n\n" . $this->label . ' (' . __( 'Type', 'gravityforms' ) . '): ' . $type : '';
					
					$value .= ! empty( $status ) ? "\n\n" . $this->label . ' (' . __( 'Status', 'gravityforms' ) . '): ' . $status : '';
					break;

				default :
				
					// var_dump($id);
					
					// var_dump(wp_get_attachment_url($id));
				
					$value = "<a href='$url' target='_blank' title='" . __( 'Download', 'gravityforms' ) . "'><img src='".wp_get_attachment_image_src($id, 'medium')[0]."' width='".wp_get_attachment_image_src($id, 'medium')[1]."' /></a>";
					$value .= ! empty( $title ) ? "<div>Title: $title</div>" : '';
					$value .= ! empty( $caption ) ? "<div>Caption: $caption</div>" : '';
					$value .= ! empty( $description ) ? "<div>Description: $description</div>" : '';
					
					$type = get_term_by('slug', $type, 'document-type')->name;
					
					$value .= ! empty( $type ) ? "<div>Type: $type</div>" : '';

// removed at this time					
/*
					if( 'approved' == strtolower( $status ) ){
						$status_icon = '<span class="dashicons-before dashicons-yes"></span>';
					}else if( 'pending' == strtolower( $status ) ){
						$status_icon = '<span class="dashicons-before dashicons-warning"></span>';
					}else{
						$status_icon = '<span class="dashicons-before dashicons-no-alt"></span>';
					}
*/
					
					$status = get_term_by('slug', $status, 'document-status')->name;
					
					$value .= ! empty( $status ) ? "<div class='".esc_attr( $status )."'>Status: $status</div>" : '';

					$value .= "<a href='$url' target='_blank' title='" . __( 'Download', 'gravityforms' ) . "' class='button button-large button-primary'>".esc_html__('Download Original','gravityforms')."</a>";
					
					$secondary_url = wp_get_attachment_url($secondary_id);
					
					$value .= "<a href='$secondary_url' target='_blank' title='" . __( 'Download', 'gravityforms' ) . "' class='button button-large button-primary'>".esc_html__('Download PDF','gravityforms')."</a>";

					break;
			}
		}

		return $value;
	}

	public function get_value_submission( $field_values, $get_from_post_global_var = true ) {

		$value[ $this->id . '.1' ] = $this->get_input_value_submission( 'input_' . $this->id . '_1', $get_from_post_global_var );
		$value[ $this->id . '.4' ] = $this->get_input_value_submission( 'input_' . $this->id . '_4', $get_from_post_global_var );
		$value[ $this->id . '.7' ] = $this->get_input_value_submission( 'input_' . $this->id . '_7', $get_from_post_global_var );
		
		$value[ $this->id . '.10' ] = $this->get_input_value_submission( 'input_' . $this->id . '_10', $get_from_post_global_var );

		return $value;
	}
	
	public function get_value_export( $entry, $input_id = '', $use_text = false, $is_csv = false ) {
		if ( empty( $input_id ) ) {
			$input_id = $this->id;
		}

		if ( absint( $input_id ) == $input_id ) {

			$value = rgar( $entry, $input_id );
			
			$ary = explode( '|:|', $value );
			
			$value = array();
			// type
			$value[$input_id . '.10'] = count( $ary ) > 4 ? $ary[4] : '';
			// status
			$value[$input_id . '.11'] = count( $ary ) > 5 ? $ary[5] : '';
			
			$upload = 'Document Type: '.$value[$input_id.'.10'].' Document Status: '.$value[$input_id.'.11'];
			
			return $upload;
		} else {

			return rgar( $entry, $input_id );
		}
	}
	
	/**
	 * Gets merge tag values.
	 *
	 * @since  Unknown
	 * @access public
	 *
	 * @uses GFCommon::to_number()
	 * @uses GFCommon::to_money()
	 * @uses GFCommon::format_variable_value()
	 *
	 * @param array|string $value      The value of the input.
	 * @param string       $input_id   The input ID to use.
	 * @param array        $entry      The Entry Object.
	 * @param array        $form       The Form Object
	 * @param string       $modifier   The modifier passed.
	 * @param array|string $raw_value  The raw value of the input.
	 * @param bool         $url_encode If the result should be URL encoded.
	 * @param bool         $esc_html   If the HTML should be escaped.
	 * @param string       $format     The format that the value should be.
	 * @param bool         $nl2br      If the nl2br function should be used.
	 *
	 * @return string The processed merge tag.
	 */
	public function get_value_merge_tag( $value, $input_id, $entry, $form, $modifier, $raw_value, $url_encode, $esc_html, $format, $nl2br ) {
		if ( empty( $input_id ) ) {
			$input_id = $this->id;
		}

		if ( absint( $input_id ) == $input_id ) {

			$ary = explode( '|:|', $value );
			
			$value = array();
			// type
			$value[$input_id . '.10'] = count( $ary ) > 4 ? $ary[4] : '';
			// status
			$value[$input_id . '.11'] = count( $ary ) > 5 ? $ary[5] : '';
			
			$upload = '';
			
			if( $value[$input_id.'.10'] &&  $value[$input_id.'.11']  ){
				$upload = 'Document Type: '.$value[$input_id.'.10'].' Document Status: '.$value[$input_id.'.11'];	
			}
			
			
			
			return $upload;
		} else {

			return rgar( $entry, $input_id );
		}
	}
	
	
}// end Class

GF_Fields::register( new Doc_Uploader_GF_Field() );


// added for later reference


add_action( 'gform_field_standard_settings', 'my_standard_settings', 10, 2 );
function my_standard_settings( $position, $form_id ) {
 
    //create settings on position 25 (right after Field Label)
    if ( $position == 25 ) {
        ?>
        <li class="encrypt_setting field_setting">
            <label for="field_admin_label">
                <?php esc_html_e( 'Encryption', 'gravityforms' ); ?>
                <?php gform_tooltip( 'form_field_encrypt_value' ) ?>
            </label>
            <input type="checkbox" id="field_encrypt_value" onclick="SetFieldProperty('encryptField', this.checked);" /> encrypt field value
        </li>
        <?php
    }
}
 
add_action( 'gform_editor_js', 'editor_script' );
function editor_script(){
    ?>
    <script type='text/javascript'>
        //adding setting to fields of type "text"
        fieldSettings.text += ', .encrypt_setting';
 
        //binding to the load field settings event to initialize the checkbox
        jQuery(document).bind('gform_load_field_settings', function(event, field, form){
            jQuery('#field_encrypt_value').attr('checked', field.encryptField == true);
        });
    </script>
    <?php
}
 
add_filter( 'gform_tooltips', 'add_encryption_tooltips' );
function add_encryption_tooltips( $tooltips ) {
   $tooltips['form_field_encrypt_value'] = "<h6>Encryption</h6>Check this box to encrypt this field's data";
   return $tooltips;
}